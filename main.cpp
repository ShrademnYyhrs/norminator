//------------------------------------------------------------------------------
//
// main.cpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#include <QApplication>
#include <QProcess>

#include "MainWindow.hpp"

static bool	isUpdatable()
{
	QProcess	process;
	QByteArray	data;

	process.start("../SoftwareMaintenanceTool --checkupdates");
	process.waitForFinished();
	if (process.error() != QProcess::UnknownError)
		return (false);
	data = process.readAllStandardOutput();
	if (data.isEmpty())
		return (false);
	return (true);
}

int main(int argc, char *argv[])
{
	if (isUpdatable())
	{
		QProcess::startDetached("../SoftwareMaintenanceTool", {"--updater"});
		return (0);
	}

	QApplication	application(argc, argv);
	MainWindow		window;

	window.show();
	return (QApplication::exec());
}
