//------------------------------------------------------------------------------
//
// Settings.tpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#include "Settings.hpp"

template<typename Key>
Settings::Return<Key, QVariant>Settings::value(Key key, const QVariant &defaultValue) const
{
return _settings.value(QMetaEnum::fromType<Key>().valueToKey(key), defaultValue);
}

template<typename Key>
Settings::Return<Key, void>Settings::setValue(Key key, const QVariant &value)
{
_settings.setValue(QMetaEnum::fromType<Key>().valueToKey(key), value);
}

template<typename Key>
Settings::Return<Key, void>Settings::beginGroup(Key group)
{
_settings.beginGroup(QMetaEnum::fromType<Key>().valueToKey(group));
}

template<typename Key>Settings::Return<Key, QVariantMap>Settings::groupValues(Key group)
{
return groupValues(QMetaEnum::fromType<Key>().valueToKey(group));
}

template<typename Key>
Settings::Return<Key, void>Settings::setGroupValues(Key group, const QVariantMap &values)
{
groupValues(QMetaEnum::fromType<Key>().valueToKey(group), values);
}

template<typename Key>
Settings::Return<Key, void>Settings::restoreValue(Key key, QComboBox * combobox, const QStringList defaultValues) const
{
restoreValue(QString(QMetaEnum::fromType<Key>().valueToKey(key)), combobox, defaultValues);
}

template<typename Key>
Settings::Return<Key, void>Settings::saveValue(Key key, const QComboBox * combobox)
{
saveValue(QString(QMetaEnum::fromType<Key>().valueToKey(key)), combobox);
}

template<typename Key>
Settings::Return<Key, void>Settings::restoreValue(Key key, QListWidget * listWidget, const QStringList defaultValues) const
{
restoreValue(QString(QMetaEnum::fromType<Key>().valueToKey(key)), listWidget, defaultValues);
}

template<typename Key>
Settings::Return<Key, void>Settings::saveValue(Key key, const QListWidget * listWidget)
{
saveValue(QString(QMetaEnum::fromType<Key>().valueToKey(key)), listWidget);
}
