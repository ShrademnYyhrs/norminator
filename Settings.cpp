//------------------------------------------------------------------------------
//
// Settings.cpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#include "Settings.hpp"

Settings::Settings(QObject *parent): _settings(qAppName().append(".ini"), QSettings::IniFormat, parent)
{
}

Settings::~Settings() = default;

QVariant Settings::value(const QString key, const QVariant &defaultValue) const
{
return _settings.value(key, defaultValue);
}

void Settings::setValue(const QString &key, const QVariant &value)
{
_settings.setValue(key, value);
}

void Settings::beginGroup(const QString &group)
{
_settings.beginGroup(group);
}

void Settings::endGroup()
{
_settings.endGroup();
}

QVariantMap Settings::groupValues(const QString group)
{
QVariantMap result;

beginGroup(group);
for (auto &key: allKeys())
result[key] = _settings.value(key);
endGroup();
return result;
}

void Settings::setGroupValues(const QString &group, const QVariantMap &values)
{
beginGroup(group);
for (auto keyValue = values.keyValueBegin(); keyValue != values.keyValueEnd(); ++keyValue)
_settings.setValue((*keyValue).first, (*keyValue).second);
endGroup();
}

QStringList Settings::allKeys() const
{
return _settings.allKeys();
}

void Settings::remove(const QString &key)
{
_settings.remove(key);
}

void Settings::restoreState(QMainWindow *mainWindow) const
{
_settings.value(Geometry);
mainWindow->restoreGeometry(_settings.value(Geometry).toByteArray());
mainWindow->restoreState(_settings.value(State).toByteArray());
}

void Settings::saveState(const QMainWindow *mainWindow)
{
_settings.setValue(Geometry, mainWindow->saveGeometry());
_settings.setValue(State, mainWindow->saveState());
}

void Settings::restoreValue(const QString &key, QComboBox *combobox, const QStringList defaultValues) const
{
combobox->addItems(_settings.value(key, defaultValues).toStringList());
}

void Settings::saveValue(const QString &key, const QComboBox *combobox)
{
QStringList items;

for (int index = 0; index < combobox->count(); ++index)
items << combobox->itemText(index);
_settings.setValue(key, items);
}

void Settings::restoreValue(const QString &key, QListWidget *listWidget, const QStringList defaultValues) const
{
listWidget->addItems(_settings.value(key, defaultValues).toStringList());
}

void Settings::saveValue(const QString &key, const QListWidget *listWidget)
{
QStringList items;

for (int index = 0; index < listWidget->count(); ++index)
items << listWidget->item(index)->text();
_settings.setValue(key, items);
}
