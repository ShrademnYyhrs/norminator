//------------------------------------------------------------------------------
//
// ComboForm.cpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#include "ComboForm.hpp"

ComboForm::ComboForm(const QString &label, QWidget *parent):
	QWidget(parent)
{
	setupUi(this);
	textLabel->setText(label);
}
