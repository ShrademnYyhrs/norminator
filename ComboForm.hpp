//------------------------------------------------------------------------------
//
// ComboForm.hpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#ifndef COMBOFORM_HPP
#define COMBOFORM_HPP

#include "ui_ComboForm.h"

class ComboForm: public QWidget, private Ui::ComboForm
{
	Q_OBJECT

public:
	explicit ComboForm(const QString &label, QWidget *parent = nullptr);
	~ComboForm() = default;
};

#endif // COMBOFORM_HPP
