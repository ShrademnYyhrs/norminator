//------------------------------------------------------------------------------
//
// Settings.hpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QComboBox>
#include <QCoreApplication>
#include <QListWidget>
#include <QMainWindow>
#include <QMetaEnum>
#include <QSettings>

class Settings
{
public:
template<typename Key, typename T>
using Return = typename QtPrivate::QEnableIf<QtPrivate::IsQEnumHelper<Key>::Value, T>::Type;

Settings(QObject *parent = nullptr);
virtual ~Settings();

QVariant										value(const QString key, const QVariant &defaultValue = QVariant()) const;
template<typename Key> Return<Key, QVariant>	value(Key key, const QVariant &defaultValue = QVariant()) const;
void											setValue(const QString &key, const QVariant &value);
template<typename Key> Return<Key, void>		setValue(Key key, const QVariant &value);

void										beginGroup(const QString &group);
template<typename Key> Return<Key, void>	beginGroup(Key group);
void										endGroup();

QVariantMap										groupValues(const QString group);
template<typename Key> Return<Key, QVariantMap>	groupValues(Key group);
void											setGroupValues(const QString &group, const QVariantMap &values);
template<typename Key> Return<Key, void>		setGroupValues(Key group, const QVariantMap &values);

QStringList allKeys() const;

void remove(const QString &key);

void	restoreState(QMainWindow *mainWindow) const;
void	saveState(const QMainWindow *mainWindow);

void										restoreValue(const QString &key, QComboBox *combobox, const QStringList defaultValues) const;
template<typename Key> Return<Key, void>	restoreValue(Key key, QComboBox *combobox, const QStringList defaultValues) const;
void										saveValue(const QString &key, const QComboBox *combobox);
template<typename Key> Return<Key, void>	saveValue(Key key, const QComboBox *combobox);
void										restoreValue(const QString &key, QListWidget *listWidget, const QStringList defaultValues) const;
template<typename Key> Return<Key, void>	restoreValue(Key key, QListWidget *listWidget, const QStringList defaultValues) const;
void										saveValue(const QString &key, const QListWidget *listWidget);
template<typename Key> Return<Key, void>	saveValue(Key key, const QListWidget *listWidget);

private:
const QString	Geometry{"Geometry"};
const QString	State{"State"};

QSettings _settings;
};

#include "Settings.tpp"

#endif // SETTINGS_HPP
