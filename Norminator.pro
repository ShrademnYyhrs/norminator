#-------------------------------------------------
#
# Project created by Shrademn
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Norminator
TEMPLATE = app

CONFIG += c++17
RC_FILE = details.rc

DESTDIR = $$PWD/bin

SOURCES += main.cpp \
    MainWindow.cpp \
    Settings.cpp \
    ComboForm.cpp

HEADERS += \
    MainWindow.hpp \
    Settings.hpp \
    Settings.tpp \
    ComboForm.hpp

FORMS += \
    MainWindow.ui \
    ComboForm.ui

RESOURCES += resources.qrc

DISTFILES += details.rc

include(theme/Theme.pri)
include(widget/Widget.pri)
