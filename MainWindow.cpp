//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#include <QCheckBox>
#include <QDebug>
#include <QFormLayout>
#include <QLabel>
#include <QSpinBox>

#include "ComboForm.hpp"
#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow(parent)
{
	setupUi(this);
	_settings.restoreState(this);
	connectWidgets();
}
MainWindow::~MainWindow()
{
	_settings.saveState(this);
}

void MainWindow::connectWidgets()
{
	connect(configurationPathLineEdit, &PathLineEdit::textChanged, this, &MainWindow::refreshConfiguration);
}

void MainWindow::refreshConfiguration(const QString &path)
{
	_configuration = Configuration{new QSettings{path, QSettings::IniFormat}};

	while (configurationToolBox->count())
		configurationToolBox->removeItem(0);

	QMap<QString, QStringList> keyMap;

	for (const auto &key: _configuration->allKeys())
		if (key.at(0) != '#')
			keyMap[key.split('_').first()] << key;

	int index{0};

	for (const auto &key: keyMap.keys())
	{
		auto	*page{new QWidget{configurationToolBox}};
		auto	*form{new QFormLayout};

		for (const auto &value: keyMap.value(key))
		{
			qDebug() << _configuration->value(value);
			form->addRow(value, createInputWidget(value));
		}
		page->setLayout(form);
		configurationToolBox->insertItem(index, page, key);
		++index;
	}
}

QWidget *MainWindow::createInputWidget(const QString &key)
{
	auto value{_configuration->value(key).toString().split(' ').first()};

	if (value.front().isDigit())
	{
		auto *widget{new QSpinBox{}};

		widget->setValue(value.toInt());
		return widget;
	}
	else
	{
		auto *widget{new QComboBox{}};

		if (value == "true" or value == "false")
			widget->addItems({"true", "false"});
		else
			widget->addItems({"ignore", "add", "remove", "force"});
		widget->setCurrentText(value);
		return widget;
	}
}
