//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2019-03-27
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "Settings.hpp"

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	void connectWidgets();

	void	refreshConfiguration(const QString &path);
	QWidget *createInputWidget(const QString &key);

	using Configuration = QSharedPointer<QSettings>;

	Settings		_settings;
	Configuration	_configuration;
};

#endif // MAINWINDOW_HPP
